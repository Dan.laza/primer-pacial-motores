using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tiempo : MonoBehaviour
{
    public float timer = 60;

    public Text textoTiempo;

    void Update()
    {
        timer -= Time.deltaTime ;

        textoTiempo.text = "" + timer.ToString("f0");

        if(timer<0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}

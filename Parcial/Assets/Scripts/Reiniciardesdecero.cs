using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reiniciardesdecero : MonoBehaviour
{
    bool isRestart = false;

    void start()
    {
        update();
    }

    void update()
    {
        move();
    }

    void move()
    {
        isRestart = Input.GetButtonDown("Restart");

        if (isRestart == true)
        {
            reiniciarNivel();
        }
    }

    public void reiniciarNivel()
    {
        SceneManager.LoadScene("Parcial");

    }
}

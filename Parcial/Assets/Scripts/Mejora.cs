using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mejora : MonoBehaviour
{
    public float masvelocidad = 15f;
    public float mastama�o = 2.5f;

    private void OnTriggerEnter(Collider other)
    {
        MovimientoCompleto movementController = other.GetComponent<MovimientoCompleto>();

        if (movementController != null)
        {
            movementController.masVelocidad(masvelocidad);
            movementController.masTama�o(mastama�o);
            gameObject.SetActive(false);
        }
    }
}
